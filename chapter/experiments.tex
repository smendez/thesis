\hypertarget{evaluating-the-network}{%
\chapter{Evaluating the Network}\label{evaluating-the-network}}

Now that we have a working convolutional network, we can start training
it and evaluating its performance. To start, we will try it out on the
starting example of this thesis: classifying the screws.

The network itself will be very simple, following the concept of
LeNet\autocite{LeCun1998} and O-CNN\autocite{Wang2017}. After each
convolution we apply a \emph{ReLU} as activation function, followed by
batch normalisation to reduce the so-called internal-covariate-shift. We
will use as \emph{basic unit} the combination of ``convolution + batch
normalisation + ReLU + max pooling'', and call it \(B_l\) if the
convolution if applied at height \(l\) of the octree. The number of
output feature maps (depth) for each \$B\_l = 2\^{}\{max(1, 9-l)\}, with
a kernel of size \(3\) and a pooling stride of \(2\). We will call a
network with maximum resolution \(2^d\) \(N(d)\), and it will have the
following form:

\[ input \to B_d \to B_{d-1} \to \cdots \to B_{2}.\]

For the task of shape classification, we add at the end of the network
two Dropout and Fully Connected (FC) layers, followed by a softmax
estimator for \(C\) classes, i.e.:

\[ N(d) \to Dropout \to FC(128) \to Dropout \to FC(C) \to softmax. \]

We use standard cross-entropy loss for training with a batch size of 32
using Adam, and let it run for 40 epochs. The initial learning rate is
\(0.001\) and the dropout rate \(0.5\).

The subset of screws consists 4360 examples, partitioned between
training and evaluation sets, and we augment it by rotating each model
uniformly along the vertical axis for a total of 12 poses.

After evaluating it up to a resolution of 256, we get the following
graph:

\includegraphics{img/screws-ocnn.png}\{ width= 100\% \}

We can see that our assumptions were correct: if we increase the
resolution, we can regain the details of the shape of the screws, and
increase the accuracy of the classifier. The accuracy keeps increasing
with the resolution until 128, where it reaches the maximum of 85\%.
After that it starts dropping; we assume that there where not enough
training examples to train the network successfully and therefor its
efficacy decreases.

Now we can run the same setup over the whole McMaster-Carr dataset.
Feuillard\autocite{Raul2018} showed that many hyper-parameters, such as
pretraining, number of sampler per class, batch size, etc. had
relatively minor impact on the training result, as well as showing that
the classification accuracy when considering each other of the major
classes (except for screws) was near perfect, so we won't try our
network with those subsets. When we use the whole dataset with the whole
31 classes, with the same parameters as above, we get the following
results:

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{img/mccarr.png}
\caption{Maximum evaluation accuracy per resolution on all 31 classes of
the McMaster-Carr parts data set}
\end{figure}

The maximum accuracy \(92.3\%\) was reached with a resolution of 256,
even though the difference to the accuracy (\(89.9\%\)) at a resolution
of 64 was not that much higher. It is also interesting to see that even
at a resolution of 8 and 16 it has a relatively passable accuracy at
\(65\%\) and \(72\%\) respectively.

\hypertarget{modelnet-datasets}{%
\section{ModelNet Datasets}\label{modelnet-datasets}}

To further validate the usefulness of the octree network, we also try it
on the more standard ModelNet datasets and compare it to other 3D
convolutional classifiers. The ModelNet data sets are a collection of 3D
models compiled by Princeton ModelNet project. To build the core of the
dataset, they compiled a list of the most common object categories in
the world, using the statistics obtained from the \emph{SUN database}.
They offer two variants, both classified and cleaned by hand: ModelNet10
and ModelNet40, differing by the number of classes, and that ModelNet10
was also oriented by hand.

\begin{figure}
\centering

\subfloat[Resolution
8]{\includegraphics[width=0.25\textwidth]{img/bath-8.png}}
\subfloat[Resolution
16]{\includegraphics[width=0.25\textwidth]{img/bath-16.png}}
\subfloat[Resolution
32]{\includegraphics[width=0.25\textwidth]{img/bath-32.png}}

\caption{Same bathtub voxelised at different resolutions. Notice how
many details get lost at lower resolutions, but the overall shape is
still recognisable.}

\label{fig:bath}

\end{figure}

We use the same experimental setup as with the McMaster-Carr data.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{img/model10.png}
\caption{Evaluation accuracies on ModelNet10 compared to a network using
dense voxels.}
\end{figure}

We can see that higher resolutions correlate with higher accuracy, with
the maximum being at 256 with an accuracy of 91.9\%, but it does seem to
taper off after resolution 64, suggesting that after this point, further
details are not necessary to distinguish objects.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{img/model10-confusion.png}
\caption{Confusion matrix for ModelNet10 at resolution 32}
\end{figure}

The confusion matrix sheds more light into the difficult categories and
where the network is having trouble. We can see that the most
misclassifications come from confusing a dresser for a nightstand, and a
desk for a table, while beds and chairs are recognised almost perfectly.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{img/model40.png}
\caption{Evaluation accuracies on ModelNet40.}
\end{figure}

Using the dataset ModelNet40, it reaches its maximum accuracy 90.1\% at
a resolution of 64 and then dropping off. We suspect this is because of
not enough data.

\begin{longtable}[]{@{}ll@{}}
\caption{Comparison table of differen voxel based 3D classification
methods}\tabularnewline \midrule
\toprule
Network & Accuracy\tabularnewline \midrule
\midrule
\endfirsthead
\toprule
Network & Accuracy\tabularnewline \midrule
\midrule
\endhead
VoxNet (\(32^3\)) & 82\%\tabularnewline \midrule
SubVolSup (\(32^3\)) & 87.2\%\tabularnewline \midrule
FPNN (\(64^3\)) & 87.5\%\tabularnewline \midrule
FPNN+normal (\(64^3\)) & 88.4\%\tabularnewline \midrule
VRN (\(32^3\)) & 89.0\%\tabularnewline \midrule
Octree N(3) & 85.2\%\tabularnewline \midrule
Octree N(4) & 88.5\%\tabularnewline \midrule
Octree N(5) & 89.9\%\tabularnewline \midrule
Octree N(6) & \textbf{90.1\%}\tabularnewline \midrule
Octree N(7) & 89.5\%\tabularnewline \midrule
Octree N(8) & 89.5\%\tabularnewline \midrule
\textbf{RotationNet} & \textbf{97.37\%}\tabularnewline \midrule
\bottomrule
\end{longtable}

As we can see on the table, our octree-based network with a resolution
of 64 compares favorably againts other voxel-based convolutional
classification methods. However, I added the very recent view-based
\emph{RotationNet} to highlight its impressive score, which currently
sits at the top of the leaderboard for the ModelNet40 classification
task.
