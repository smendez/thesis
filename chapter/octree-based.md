# Octree-Based Convolutional Neural Networks
Now that we saw the limitations of a dense VoxNet, we need to find a way to use space in an efficient manner.

A key insight into voxel-based decomposition is that the shapes are **sparse**, i.e. the **occupancy** of objects in their bounding box is usually very low. This means that most of the voxels of the grid are empty and not describing any interesting feature of the object.

Ideally, one would want a way of representing and storing only the voxels with useful information for the learning task. This is where the **Octree** data structure comes in. The main difficulty now is how to represent the Octree efficiently and describe the convolution on the Octree such that they are computationally efficient and can take advantage of parallel hardware. The following implementation and solution to these problems are based on the *Caffe* implementation of *O-CNN*[@Wang2017].

![The same chair model from the ModelNet10 data set voxelised at different resolutions. As we can see, the higher the resolution, the fewer voxels actually contain information about the object compared to the total amount of voxels. At $128^3$ only around 2% is actually not empty.](img/occupancy.png){ width=100% }

First we will introduce the concept of an *Octree*, then we will describe how it can be used in the context of a 3D CNN.


## Octree {#sec:Octree}

An **Octree** is the three-dimensional analogue of a **Quadtree**. This means it is a tree-based data structure which recursively and hierarchically subdivides space into eight octants.In the tree representation this implies that each internal node has exactly eight children, and the leaf nodes contain the actual data. The criterion for the subdivision of an octant is if there is (at least) a point inside of it, in this case, this octant is subdivided recursively, otherwise, this octant becomes a leaf node.

Octrees are widely used in Computer Graphics, since it allows efficient storage and lookup of spacial bodies, specially if the shape only sparsely populates the surrounding space, which in most cases is the *minimal axis-aligned bounding box*.

## Building the Octree

To begin, we scale the 3D shape into an axis-aligned bounding 3D unit cube. Then we subdivide recursively in a breadth-first manner the cube into octants. For each child octant, we check if it is occupied. If it is, then this octant will be subdivided further. Each level of the tree corresponds to a *layer* or *level*, so that at depth $l$, then the next *layer*, which will contain smaller octants, will be at depth $l + 1$. This process is continued for every non-empty octant until a pre-determined depth $d$ is reached. Note that this also determines the maximum resolution of the voxelised grid in the end.

Along the actual tree, we need to collect extra data about the structure so that we can refer to specific voxels later, and easily calculate the children, parents, and neighbours of a voxel, which we will need for the network operations. These two structural properties of each node are the *[shuffle] key* and the *node label*.

We use the *shuffle key*[@Wilhelms1990] of an octant (this includes inner nodes, not just leaves) to encode its position in space, and also to sort them in a canonical way. For an octant $O$ at depth $l$, we assign to it an unique $3*l$-bit string $key(O) := x_1y_1z_1x_2y_2z_2\ldots x_ly_lz_l$, where $x_i, y_i, z_i \in {0, 1}$ are the *octant* coordinates of the voxel.
To see why it makes sense to notate it like this, consider that a any level, an inner node can have eight children, which we can label from $0$ to $7$. If we write this in binary, each number describes uniquely which sub-octant of the parent they cover, following the convention $xyz$, and that `1` means "upper" value in that dimension. For example, the number `001` means the octant in the lower $x$ and $y$ coordinates, but in the upper $z$ region (by lower/upper we mean back/front for $z$, bottom/top for $y$, and left/right for $x$, for a fixed coordinate system and orientation). This means that concatenating these triplets, where each one represents a $layer$, we can pinpoint the coordinates of every octant. An nice property we get "for free" in this representation, is that for every node, the direct children are contiguous when sorted in lexicographical order. We store each key in a 32-bit integer in our implementation.

While we can use the shuffle key to find the position of a node, we still need to be able to determine its parent, so that we can downsample during pooling operations. To this end, we use the *node label*. For each non-empty node, we give it the number label $p$ if it is the $p$-th non-empty node at that depth $l$, given that the nodes are sorted using their shuffle keys. If the node is empty, we mark it with $-1$ (in our implementation, could be $0$ depending on indexing convention).
Since only non-empty nodes are subdivided, we can easily calculate the index of the children of a non-empty node. For example, if a node $j$ has the label $L_j$, then its first children at the next level will have index $8 * L_j$. We know from the sorted shuffle key that the children are continuous, so we now know the location of *all* of the node's children.

![Illustration of the octree structure using a 2D quadtree analogue[@Wang2017] of depth 2. On the left panel, the red line represents the object we are observing. The $l$ on the left indicates the current octree level. Squares that are not occupied are empty nodes. The numbers in the squares represent the shuffle key in decimal notation for that level. The middle panel shows how we store the *shuffle keys* in memory. Each level gets a array the length of how many nodes we subdivided into. This means we also need an extra control array that stores their length. On the rightmost panel we see that the *node labels* are stored in a similar way. Each number $p$ means that that node is the $p$-th non-empty node, when the keys are sorted lexicographically. Here empty nodes are denoted with a $0$ since the indices start at $1$. In our implementation, our arrays are $0$-indexed.](img/ocnn.png){ width=100% }

These structure descriptors are stored individually in contiguous arrays per level, as shown in the picture. As for the input signal, the feature vector is stored in the corresponding position for non-empty leaf nodes, while empty nodes simply get a zero vector.

Bear in mind that this implementation of an octree is so-called *pointer-less*, which means that the tree structure is implicit in the values of the array, and not given directly by pointers to children and parents. This reduces the memory footprint.

![Upper row shows the same bed voxelised at different resolutions. Middle row is the same as the upper row, but orthogonally viewed from the side, with a grid superimposed to compare with the lower row. The lower row voxelises the bed at the same resolution but with an octree structure. Darker colours represent deeper octree levels. Note that in our implementation, we don't take into account inner voxels, just the surface.](img/octree-dense.png){ width=70% }

## Operations on the Octree {#sec:Octree-Ops}

Now that we have the actual structure of the octree, we have to define *how* the common convolutional network operators, namely the convolution itself and pooling, will work on it. First we will look at the convolution.

The best way to visualise how to compute the convolution is to see the equation at an octant in its unrolled form:

$$ \Phi(O) = \sum_x\sum_y\sum_z W_{xyz}F(O_{xyz}), $$

where $O_{xyz}$ enumerates the neighbourhood arount the octant $O$ in its same layer, $F$ is the corresponding feature vector, and $W_{xyz}$ the weights of the kernel. This formulation is equivalent to the very efficient *GEMM* routine that can run on a GPU.

The difficulty now is determining the neighbours of our octant. Since we have the shuffle key of the octant, we can easily determine its integer coordinates $(x, y, z)$, and from there, we can get the coordinates of the neighbouring octants in constant time, thus giving us their shuffle keys. But now we don't know their individual positions in the structural arrays mentioned above. Following the method in [@Wilhelms1990a], this can be computed solely from the key. However, it is very expensive. A good compromise is to precompute a hash-table mapping each octant's key to its position (index) on the feature arrays. This gives us amortised constant time access to the feature vectors.
We do this for each of the $K^3 - 1$ neighbours, where $K$ is the kernel size, and we can compute the convolution normally now.

If the stride of the convolution operator is larger than $1$, this means that down-sampling is taking place, and we have to store the result in higher layers. The arrays of *node labels* can restore this correspondence. We must also remember that the array of feature vectors has to be resized too.

The same discussion about down-sampling applies to *pooling* too. Conceptually, pooling on the octree is very simple. A typical max-pooling kernel has size 2 and a stride of 2. This means a block of eight elements. The eight children of an octant are stored consecutively in memory. Therefore, this max-pooling is equivalent to finding the maximum value of every (disjoint) eight contiguous elements, and storing it in the parent node.

It should be noted that in contrast to *OctNet*[@Riegler2016a], we only consider non-empty nodes when doing the convolution, and *only* propagate information along non-empty nodes. While this surely saves up on computational resources, there is now hard justification on why this is a qualitative improvement. Nevertheless, it is suggested by Wang et al.[@Wang2017] to be a useful heuristic.

Now that we know how such a network works, we can see idiosyncrasies of implementing it on TensorFlow.
