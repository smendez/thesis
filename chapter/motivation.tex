\hypertarget{sec:Motivation}{%
\chapter{Machine Learning in CAE}\label{sec:Motivation}}

Worldwide engineering workflows have benefited greatly from tighter
integration with \emph{Computer Aided Engineering} (CAE) methods. These
are taking advantage of ever increasing computational power and
accumulated data to help in every step of the process from planning and
design, all the way to operations and simulation. Machine Learning tools
can be used to take over expert tasks, and it is in this space where we
want to contribute with this thesis.

Engineers often work with \emph{Computer Aided Design} software to
create and manipulate many complex mechanical parts and assemblies,
which can contain thousands of these parts. Normally, each individual
model in these assemblies is labeled with important information,
including part type, manufacturer, suppliers, material, etc.

In many situations, however, these labels could be missing, either from
getting lost, or deleted on purpose. In the former case, this can happen
accidentally, when data is sent from individual actor to actor in the
Product Lifecycle Management pipeline, which can deal with different
file data formats, or CAD software. In the latter case, the manufacturer
or supplier may want to protect the details of their intellectual
property and therefore omit certain data in their files. In any case, it
is definitely an advantage to be able to (re-)identify unlabeled data.

This is the problem we are trying to solve. Conveniently, in the past
decade, Machine Learning method have worked exceptionally well in the
field of image classification. Since 2012, methods based on deep
learning have beat others in the ImageNet Large Scale Visual Recognition
Competition (ILSVRC), and the models have only gotten better since the
introduction of \emph{Convolutional Neural Networks}. These methods have
also performed excellently in tasks like person identification and image
analysis, as well as in the field of engineering analysing part stress,
fatigue and predicting failure.

Nevertheless, the successes of 2D image processing have not yet been
replicated at same level with 3D model, which is the domain we are
interested in. This is due to a number of reasons: first, deep learning
methods require a very large amount of data. This is easily available
for 2D images from a number of exemplary benchmark data sets. This is
simply not the case for 3D models, due in part to the many different
data formats and difficulty of acquiring data. The number of good,
clean, universally used data sets is rather small, and more importantly,
there is none based on mechanical parts. Secondly, while the resolution
of an image increases quadratically with its width or height, a
discretised 3D model increases cubically. Current computing power is not
enough if we want to process 3D at a sufficient amount of detail, so we
have to develop more sophisticated methods.

In this thesis we will focus on solving both of these issues. We will
present and process a large data set mechanical parts from Raúl
Feuillard's Master Thesis\autocite{Raul2018} based on the McMaster-Carr
parts catalog. Then we will use an \emph{Octree Based Convolutional
Neural Network} implemented in TensorFlow based on the work by Wang et
al.\autocite{Wang2017} to evaluate its viability on a classification and
retrieval task, and compare it with other methods and datasets. The
thesis will be structured as follows:

\hypertarget{outline}{%
\section{Outline}\label{outline}}

In Chapter ~\ref{sec:Background} we will introduce basic concepts and
preliminaries needed to understand the rest of the thesis, including a
primer on Convolutional Neural Networks, TensorFlow, and Octrees.

Chapter ~\ref{sec:CNN-App} describes and discusses different approaches
used to solve 3D shape classification. Additionally, we will see in
detail the Octree based network which will be the focus of this thesis,
including its implementation details.

The next chapter offers an overview of the data sets we will use for the
evaluation of the algorithms. Chapter ~\ref{sec:Classification} explains
the setup for the classification task and its results. Chapter
~\ref{sec:Retrieval} does the same for the shape retrieval task

Finally, in Chapter ~\ref{sec:Conclusion} we summarize our conclusions,
and propose ways in which this framework can be used and extended.
