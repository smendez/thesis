# Evaluating the Network

Now that we have a working convolutional network, we can start training it and evaluating its performance. To start, we will try it out on the starting example of this thesis: classifying the screws.

The network itself will be very simple, following the concept of LeNet[@LeCun1998] and O-CNN[@Wang2017]. After each convolution we apply a *ReLU* as activation function, followed by batch normalisation to reduce the so-called internal-covariate-shift. We will use as *basic unit* the combination of "convolution + batch normalisation + ReLU + max pooling", and call it $B_l$ if the convolution if applied at height $l$ of the octree. The number of output feature maps (depth) for each $B_l = 2^{max(1, 9-l)}, with a kernel of size $3$ and a pooling stride of $2$. We will call a network with maximum resolution $2^d$ $N(d)$, and it will have the following form:

$$ input \to B_d \to B_{d-1} \to \cdots \to B_{2}.$$

For the task of shape classification, we add at the end of the network two Dropout and Fully Connected (FC) layers, followed by a softmax estimator for $C$ classes, i.e.:

$$ N(d) \to Dropout \to FC(128) \to Dropout \to FC(C) \to softmax. $$

We use standard cross-entropy loss for training with a batch size of 32 using Adam, and let it run for 40 epochs.  The initial learning rate is $0.001$ and the dropout rate $0.5$.

The subset of screws consists 4360 examples, partitioned between training and evaluation sets, and we augment it by rotating each model uniformly along the vertical axis for a total of 12 poses.

After evaluating it up to a resolution of 256, we get the following graph:

![Maximum evaluation accuracy for the screws subset. The cyan solid line is the result of the octree-based method, while pink dashed line used dense voxels.](img/screws-ocnn.png){ width= 100% }

We can see that our assumptions were correct: if we increase the resolution, we can regain the details of the shape of the screws, and increase the accuracy of the classifier. The accuracy keeps increasing with the resolution until 128, where it reaches the maximum of 85%. After that it starts dropping; we assume that there where not enough training examples to train the network successfully and therefor its efficacy decreases.

Now we can run the same setup over the whole McMaster-Carr dataset. Feuillard[@Raul2018] showed that many hyper-parameters, such as pretraining, number of sampler per class, batch size, etc. had relatively minor impact on the training result, as well as showing that the classification accuracy when considering each other of the major classes (except for screws) was near perfect, so we won't try our network with those subsets. When we use the whole dataset with the whole 31 classes, with the same parameters as above, we get the following results:

![Maximum evaluation accuracy per resolution on all 26 classes of the McMaster-Carr parts data set](img/mccarr.png){ width=100% }

The maximum accuracy $92.3\%$ was reached with a resolution of 256, even though the difference to the accuracy ($89.9\%$) at a resolution of 64 was not that much higher. It is also interesting to see that even at a resolution of 8 and 16 it has a relatively passable accuracy at $62\%$ and $75\%$ respectively.

As mentioned before, the whole dataset consists of 10703 samples, of which 8589 are using for training (as opposed to validation).
It is interesting to note, that there is a huge discrepancy on how the samples are distributed among categories. As shown on the following table, some categories contain more than thirty times as many samples as others. As Feuillard[@Raul2018] showed, this does not seem to pose a real problem for the classification, suggesting that the parts are different enough so that few examples are enough to learn the features. However, we would like to investigate how the performance decays related to the *overall* number of samples.

Category | Samples
--- | ---
L Hook Anchor | 48
Stud | 50
General Set Screw | 56
External Tooth Lock Washer | 64
Square Nut | 65
Elevator Bolt | 70
Press Fit Nut | 72
Internal Tooth Lock Washer | 78
Captive Panel and Binding Post | 84
Flange Nut | 91
Wing Nut | 113
T Slot Bolt | 130
Coupling Nut | 142
Cap Nut | 180
Split Lock Washer | 191
Socket Head Screw | 210
Socket Screw | 274
Locknut | 302
Female Hex Adapter | 348
Thumb Screw | 356
Carriage and Plow Bolt | 432
Flat Head Screw | 770
Flat Washer | 971
Rounded Head Screw | 1536
Hex Nut | 1954

Table: Categories and their amount of training samples before any data augmentation.

To test this, we remove a set percentage randomly of training samples from each category, so that the proportions stay the same, and compare the evaluation performance.

![Evaluation accuracies on the diminished dataset at different percentages.](img/mccarr-percent.png){ width=75% }

As we can see on the graph, we can remove up to 20 percent of the data while having only negligible effect on the performance. However, as we keep removing data, it keeps worsening, and at 30 percent of the original data, the evaluation accuracy degrades dramatically.

To test if this is due to the categories that have very few samples, we run two more tests: first we remove the half to the categories that had the fewest amount of samples. Coincidentally, this eliminates all categories that have less than 100 training data files. For the second test, we see how the data reduction affects the performance of only on major category, in this case `Nuts`, since it presented a very good accuracy, contrasted to screws.

![Evaluation accuracies on the diminished dataset at different percentages, after removing half the categories with the fewest members.](img/mccarr-half.png){ width=75% }

After removing the categories with less than 100 members, we can see that the degrading is not as drastic after reducing the data. This also benefits from having less categories to take into account, of course.

![Evaluation accuracies on the diminished dataset at different percentages, considering *only* `Nuts`.](img/mccarr-percent-nuts.png){ width=75% }

If we run the same setup using only `Nuts` we see a very similar behaviour, but with better performance than with all the categories.

## ModelNet Datasets

To further validate the usefulness of the octree network, we also try it on the more standard ModelNet datasets and compare it to other 3D convolutional classifiers. The ModelNet data sets are a collection of 3D models compiled by Princeton ModelNet project. To build the core of the dataset, they compiled a list of the most common object categories in the world, using the statistics obtained from the *SUN database*. They offer two variants, both classified and cleaned by hand: ModelNet10 and ModelNet40, differing by the number of classes, and that ModelNet10 was also oriented by hand.

<div id="fig:bath">
![Resolution 8](img/bath-8.png){width=25%}
![Resolution 16](img/bath-16.png){width=25%}
![Resolution 32](img/bath-32.png){width=25%}

Same bathtub voxelised at different resolutions. Notice how many details get lost at lower resolutions, but the overall shape is still recognisable.
</div>

We use the same experimental setup as with the McMaster-Carr data.

![Evaluation accuracies on ModelNet10 compared to a network using dense voxels.](img/model10.png){ width=75% }

We can see that higher resolutions correlate with higher accuracy, with the maximum being at 256 with an accuracy of 91.9%, but it does seem to taper off after resolution 64, suggesting that after this point, further details are not necessary to distinguish objects.

![Confusion matrix for ModelNet10 at resolution 32](img/model10-confusion.png){ width=75% }

The confusion matrix sheds more light into the difficult categories and where the network is having trouble. We can see that the most misclassifications come from confusing a dresser for a nightstand, and a desk for a table, while beds and chairs are recognised almost perfectly.

![Evaluation accuracies on ModelNet40.](img/model40.png){ width=75% }

Using the dataset ModelNet40, it reaches its maximum accuracy 90.1% at a resolution of 64 and then dropping off. We suspect this is because of not enough data.

Network | Accuracy
--- | ---
VoxNet ($32^3$) | 82%
SubVolSup ($32^3$) | 87.2%
FPNN ($64^3$) | 87.5%
FPNN+normal ($64^3$) | 88.4%
VRN ($32^3$) | 89.0%
Octree N(3) | 85.2%
Octree N(4) | 88.5%
Octree N(5) | 89.9%
Octree N(6) | **90.1%**
Octree N(7) | 89.5%
Octree N(8) | 89.5%
**RotationNet** | **97.37%**

Table: Comparison table of differen voxel based 3D classification methods

As we can see on the table, our octree-based network with a resolution of 64 compares favorably againts other voxel-based convolutional classification methods. However, I added the very recent view-based *RotationNet* to highlight its impressive score, which currently sits at the top of the leaderboard for the ModelNet40 classification task.


Since ModelNet is such a widespread data set, it would be nice to see if models already trained on it can be extended to correctly classify machine parts. To test this, we take the network we used in the previous task and substitute the last layer of the network. The idea behind this is that we rely on the network having learned on the other layers important features to extract from the 3D models in order to classify them. Therefore, the penultimate layer is the one that contains the actual data that does the classification (also called the *image feature vector*, even though in our case it is not an image).
Thus, we use the McMaster-Carr data to train the last layer, which outputs the categories.

![Comparison of the accuracy achieved by the full McMasterr-Carr dataset, only 30% of the dataset, and only 30% percent of the dataset but with ModelNet10 pretraining.](img/pretraining.png){ width=75% }

Usually, if the two datasets are similar, one would hope that pretraining helps if not many samples of the second dataset are available. This is why we do the experiment with 30% of the McMaster-Carr dataset. If the ModelNet10 objects are useful for pretraining, we would expect an improvement of performance. However, we see that the accuracy is only minimally better, therefore we can conclude that possibly the two datasets are so different that pretraining doesn't help.


## Shape Retrieval

Another typical application for supervised machine learning, apart from classification, is *shape retrieval*. The difference with classification, is that in this task, our goal is not to assign the evaluation sample its category, but instead find in a database of objects the one that is the most similar.
This would be essential for our proposed goal of identifying unknown 3D machine parts.

We test it both on ModelNet10 as well as the McMaster-Carr dataset, as we did in the previous section.

In order to do the retrieval, we need for each object a feature vector, so that we can compare it to the feature vectors of the objects in our database. We use our octree based network as the feature extractor, with the same network configuration as in the classification task.
For the training stage, as we did before, we minimise the cross-entropy with respect to the categories, and the output, a vector with the category probability of each category, is used as the feature vector of the object. If we do data augmentation, at this stage we would join the vectors of the different rotated samples of the same object.

After training, and during evaluation, for each query we collect from the database all shapes of the same (predicted) category, and sort their feature vectors according to each their distances from the feature vector of the query object. We can then easily get the top-N results.

We will use five metrics to evaluate the quality of the retrieval: precision, recall, mAP, F-score, and normalized discounted cumulative gain (NDCG), as used for the ShapeNet benchmarks[@Savva2016]. Here we define each of these metrics: *precision* is the fraction of relevant instances among the retrieved instances, in our case, this means the percentage of positive instances (correctly retrieved) up to this entry. *Recall*, also known as sensitivity, is the fraction of relevant instances that have been retrieved over the total amount of relevant instances, i.e. the number of positive entries up to this entry divided by the total number of objects in the category. *mAP* is the mean average precision, and the *F-score* is the harmonic mean of precision and recall, which translates to $F = \frac{1}{\frac{1}{`recall`} + \frac{1}{`precision`}}$. Lastly, *NDCG*, which we only apply to the McMaster-Carr dataset, uses a graded relevance: 3 for perfect category and subcategory match in query and retrieval, 2 for category and subcategory both being same as the category, 1 for correct category and a sibling subcategory, and 0 for no match. In our case, the category would be one of the four major categories, and subcategory what we referred to category for the classification task.

| Dataset       | P     | R     | mAP   | F-Score | NDCG  |
| ---           | ---   | ---   | ---   | ---     | ---   |
| ModelNet10    | 0.805 | 0.800 | 0.798 | 0.910   | n/a   |
| McMaster-Carr | 0.641 | 0.671 | 0.642 | 0.879   | 0.920 |

Table: Results for the retrieval task for the ModelNet10 and McMaster-Carrdatasets, both at a resolution of $128^3$.

While there isn't really anything to compare those results to, if we put them side by side to the ones from the `SHREC-16` benchmark, we can conclude that our octree networks functions in a satisfactory way.
