# Conclusion and Future Work

In this thesis, we have built a convolutional neural network for voxelised 3D shapes using an octree data structure, which can be efficiently parallelised on the GPU. We have shown that it can effectively handle higher resolution input compared to networks using dense voxelisation, and therefore achieve higher performance on classification tasks. The implementation is both efficient from a memory and computational perspective.

Furthermore, as we set out to do at the beginning, we applied this network on a new dataset of mechanical parts, such as bolts and screws. It proved successful at classification and retrieval tasks, which could be valuable in a CAE context. Again, the results were better with higher resolutions, which were only achievable due to the octree layout.

Doing the implementation in *TensorFlow* allows to take advantage of its ecosystem, including its *TensorBoard* which allows us to examine in real time all kinds of summary metrics, such as the accuracy, error function, and internal state of the neurons. We also provide further tooling in a script to calculate and show the confusion matrix of a classification task, and a script to evaluate specific instances and show their voxelised model in case it is wrongly classified.

![Screenshot of the evaluation command line tool. It shows the top-5 categories, along with their probability. In case the highest rated guess is wrong, it displays the voxelised model so the user can inspect it.](img/tool-vis.png){ width=75% }

Overall, we showed that this is a valid path to analyse mechanical part model, and is ready to be extended.

## Future Work

Now that we have laid the groundwork for processing mechanical data, there is a lot of room for improvement.

On the first front, we would need more data. Scraping the data from the McMaster-Carr catalog site can be automated, but is time consuming and error prone. However, this is vital to have a more extensive and detailed dataset, and because the classification on the site is confusing and sometimes ambiguous, a human would have to check the results. Of course, one could try to use other sources of data, and models with more than one part in it.

After that first stage, another possibility for improvement would be the preprocessing stage and pipeline. Currently, the conversion from `.OFF` file to voxels is extremely time consuming, needing multiple days just for the conversion to point clouds of the McMaster-Carr dataset. While the conversion to point cloud and then the voxelisation takes longer than doing the voxelisation directly (but it allows us for example to calculate the normals), the same point cloud file can be used for voxelisation at different resolutions, which saves time if one needs more than a fixed resolution. However, a more efficient ray caster or voxelisation scheme can be implemented.

On the subject of the octree network itself, there are two main avenues for extension. First, one could try and make the current operators more efficient, by for example creating a more coalescent memory layout scheme for the GPU, seeing as there is currently plenty of "jumping around" in memory searching for the neighbours of the octants.
An interesting avenue for research here would be alternative hierarchical subdivision schemes. There has been work showing that a triangular tessellation need fewer operations in a convolutional network for 2D images[@Graham2015]. Sadly, tetrahedra don't uniformly tessellate 3D space, however there have been suggestions for different subdivision primitives, such as the orthoschemes, an irregular tetrahedron that subdivides itself[@Debrunner1990a], and permutohedral lattices[@Adams1981]. For both of these, efficient layouts and addressing schemes would have to be developed.

It would be interesting to see how the network performs on the data in other tasks, such as segmentation. This would be specially important if the input data consisted of more complex assembled parts.

On the end user part of the pipeline, better tooling would have to be developed, and specially there is the chance of integration with the *CAEML* framework and other CAD and CAE software packages.
