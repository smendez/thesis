# Classifying 3D Mechanical Parts

In this chapter, we will present a running example we will use to introduce the different concepts and motivate this thesis: that of classifying models of screw. In the first Section @sec:Example we present the groundwork of the example, in the next Section @sec:CNN we give a primer on CNNs, and then we describe different methods to apply Convolutional Networks on our 3D data. Finally, we discuss why this leads us to the main work of this thesis.

## Screws as an example {#sec:Example}

As many other disciplines, engineering is moving towards further and further automation, now specially given the vast amounts of data and the methods to process them. This has led to the field of **Computer Aided Engineering**, which refers to the use of computational resources as a support to engineering processes; this includes such varied tools as *Computer Aided Design* (*CAD*) and *Enterprise Resource Planning* (*ERP*) software.

The specific long-term goal of this thesis, in the context of CAE is to develop a viable Machine Learning method to classify machine parts. This would have many practical applications in the engineering field, such as labeling unknown parts, or properties value interpolation, based on a database of known machine parts.

This brings us to our actual task at hand: as an illustrative example of how we could classify 3D CAD data, we will try to classify a dataset of screws into 7 different categories according to their shape and type. For more details about this dataset, see the Section @sec:Dataset on the data.

Many methods have been developed to classify 3D data based on *Convolutional Neural Networks* (CNN), which have achieved tremendous success on similar tasks, so we have many to choose from. Of course, we have to understand CNNs to able to understand the individual architectures, first we go through a primer on the general concept of how CNNs work.

## Convolutional Neural Networks {#sec:CNN}

In recent years, *Convolutional Neural Networks*, or *CNN*s, as introduced by Yann LeCun[@LeCun1998], have been extremely successful in a variety of Machine Learning benchmarks, specially image classification tasks [@Wang2017; @Maturana]. These networks have a different operation than standard *Multi-Layer Perceptrons*, even though they are based on similar principles, adapted to work on data that has a regular, known, **grid-like** topological structure. As a 1D example, we can think of discretised time-series data. In 2D, the classical example is that of images, where the grid structure is given by the pixels matrix. As for 3D, the obvious extension would be voxelized data, which presents some problems which we will discuss in later sections; another alternative is images time-series, or videos.

As the name implies, the key operation in *CNN*s is the **convolution**, in the discrete sense, which is expressed as:

$$ (x \star w)(t) = \sum^{\infty}_{a=-\infty}x(a)w(t - a) $$

where the first argument $x$ is called the **input** and in this context the second argument $w$ is called the **kernel**, or **weights**. The output is sometimes referred as **feature maps**. Typically, the kernel is much smaller than the input, which gives Convolutional Networks the property of **sparse connectivity** or **sparse interaction**. In the context of images, this means that we look only at a local neighbourhood of the input, and learn small, meaningful features such as edges. This leads to enormous gains in memory storage compared to fully connected neural networks, since we have to store much fewer parameters, and it also improves its statistical efficiency.

![Illustration of a 2D convolution on the pixel of an image. We place the 3x3 kernel on the image around the target pixel, then we multiply the overlapping entries, and finally we sum all 9 products into one entry in the new *feature map*. This process is repeated for every pixel.](img/convolution.png){ width=50% }

We should also note, that a discrete convolution can be done extremely efficiently on highly parallelizable hardware, such as a modern GPU, and is provided by many standard math libraries such as the GEMM operator in the *Eigen* Library, which we will use for the implementation of our CNN.

After the each hidden layer of convolutions, the output is typically passed through an activation function, the most popular of which is the *Rectified Linear Unit* (*ReLU*) $f(x) = max(0, x)$ which prevents output saturation.

Another common operation in CNNs is the so called **pooling** operation, which is often applied after each convolutional layer. Here local summary statistics are collected on the neighbourhood of each input cell, such as the maximum in **max pooling** or the average in **average pooling**. This helps make the representation invariant to small translations of the input, although it has been suggested that a pooling operation is in some cases not strictly necessary, and can be substituted with improved results in some instances by a strided convolution [@Springenberg2014].

![Diagram of the architecture of the AlexNet[@Krizhevsky2012a] consisting of multiple layers, finishing with a dense fully connected neural network.](img/alexnet.png){ width=100% }

Now that we understand the fundamentals, we can delve into the individual methods.

## Convolutional Methods for 3D Shapes
Considering the success and popularity of Convolutional Networks methods for 2D images, videos, and time series, among other applications, it is no surprise that researchers have attempted to apply these methods to 3D shapes.

However, the conversion from 2D to 3D is not trivial: images are already explicitly and intrinsically described as a two-dimensional matrix, and as mentioned in Section [@sec:CNN], this grid property is vital for CNNs. This is not the case for 3D shapes. First of all, there is no canonical representation, and this varies both in file types, as well as conceptualisation. Shapes can be described a set of triangles and other polygons, as in the *OFF* and *PLY* formats, they can be described as Bèzier-surfaces using *NURBS*, which is common in *CAD* softwares, as parametric equations, and as simple three-dimensional point clouds. Furthermore, most of these representations do not have an obvious implicit grid-like topological structure, which we need for the CNNs to work, nor an obvious way to extend the convolution operation to fit them.

Nevertheless, researchers have invented different approaches to try to overcome these limitations. First we will see **Multi-View** based approaches in Section [@sec:Multi-View], which forgo the 3D structure and operate on 2D "screenshots" of the shape. Other methods try to map the 3D shape into a more manageable space, which we will describe in Section [@sec:Retopo]. These involve changing the **topology** and parametrisation of the object in a way that allows the use of the convolution operations.

Finally, we will examine **Voxel**-based methods in Section [@sec:Voxel-Based]. These are the more direct analogue of the two-dimensional case, where we simply substitute pixels with voxels. Of course, this brings along its own set of problems. The method described in this thesis will try to overcome these problems.

![Visualisation of different approaches to convolutional neural networks. Clockwise from the top left: multi-view based methods, where the networks operate on 2D snapshots of the 3D object; voxel based methods, where the model is discretised three-dimensionally into a regular cubic grid; methods that operate on the surface by transforming it or remapping the coordinates into a domain where the conditions for a convolution are satified; networks that operate directly on the 3D point cloud representation.](img/methods.png){ width=100%}

### Multi-View Approaches {#sec:Multi-View}
The basic idea of **Multi-View** approaches, as introduced by [@Su2015], is to have many *virtual* cameras take 2D images of the 3D object at different positions. Then, image features are extracted from each of these rendered images (e.g. by *AlexNet*). These features are then combined across views through a pooling layer, followed by a fully connected layer, which allows the final classification.

![The architecture of a Multi-View based network. 12 rendered camera images of the object from different positions are fed as features into separate 2D convolutional networks. The results are pooled together and passed to another network which does the classification[@Su2015].](img/multi-network.png){ width=100% }

The work by [@Qi2016a] extended the method to use multi-resolution spherically discretisation, achieving an accuracy of 91.4% class average on the *ModelNet 40* data set.

A problem with this approach is handling self occluding objects, i.e. objects where a part of it covers itself, and so doesn't allow the virtual cameras to get a complete representation of the geometry. In practice this does not seem to be a significant issue.

Another difficulty is determining how many views are rendered, and under which extrinsic camera parameters. This plays a significant role on the accuracy of the task [@Su2015]. However, recently, there are approaches to learn the best views in an unsupervised way based on each image feature saliency rating [@Kanezaki2016].

### Surface Retopologisation and Reparametrisation {#sec:Retopo}
In contrast to the Multi-View based methods, the ones based on retopologisation map the shape itself into another domain where the usual convolution operations are applicable. The way this remapping happens, and into which domain, is what distinguishes one specific technique from another. To illustrate the characteristics of this approach, we will describe two different techniques.

*Geodesic convolutional networks*[@Masci2015a] are based on the **local** topology around a subset of points on the surface. To do this, the surface is sampled at $N$ points. Fanning around each of these, a triangular mesh is constructed. This patch is further discretised radially based on the *geodetic* distance from the centre, up to a set distance. The number of rings and "pies" in a subdivision of patch are hyperparameters. This creates a radial grid upon which we can use a variation of the convolution, called the geodesic convolution:

Let $\Omega(x) : B_{\rho_0}(x) \to [0, \rho_0] \times [0, 2\pi)$ denote the bijective map from the manifold into the local geodesic polar coordinates $(\rho, \theta)$ around $x$, and let $(D(x)f)(\rho, \theta) = (f \cdot \Omega^{-1}(x))(\rho, \theta)$ be the patch operator interpolating f in the local coordinates. We can regard $D(x)f$ as a ‘patch’ on the manifold, and define:

$$(f \star a)(x) = \sum_{\theta, r}a(\theta + \Delta \theta, r)(D(x)f)(\rho, \theta).$$

Note that due to the rotational symmetry, the filter is ambiguous with respect to the angular coordinate.

![Geodesic patches drawn on top of a human figure. The rightmost image shows why the convolutional filter on one such patch would be rotationally ambiguous[@Sinhab].](img/geo-patches.png){ width=70% }

Due to the nature of the mapping, it only works for water-tight surfaces of genus 0 (for example, not a doughnut).

A simpler mapping is presented in [@Sinhab], called *Geometry Images*, where the surface is projected onto a sphere, and this sphere then into an octahedron. This octahedron is the cut up, flattened, and discretised, and can then be used as input into regular convolutional networks.

This network achieved an accuracy of 88.4% and 83.9% on classification tasks on the *ModelNet 10* and *ModelNet 40* data sets respectively. Other parametrisations, such as the toric covering have been used successfully on segmentation and correspondence tasks[@Maron2017].

A common restriction of these methods is that they only work on *smooth surfaces of genus 0*, that is, surfaces that are topologically equivalent to spheres. The *sphere* restriction is not too bad in everyday objects, such as the classes in ModelNet, but it would reject, shapes with a hole in it, such as a mechanical nut. The *surface* restriction is problematic in the sense that it disqualifies model formats that don't explicitly have a manifold structure. This includes point clouds. Building the surface out of a point cloud is not a trivial, unambiguous task.

![Toric covering and coordinates[@Maron2017] drawn on a human face. This is exemplary of these approaches and shows why this method would not work unadapted for surfaces of genus not 0.](img/toroidal.png){ width=70%}

### Voxel-Based Methods {#sec:Voxel-Based}

In **Voxel**-Based methods, we take the most direct approach to generalising 2D convolutions by replacing 2D pixels with 3D voxels. This is obtained by segmenting a prismatic subset of space containing the object into a hexahedral grid (most of the times cubic). Any of the common shape representations can be discretised this way, and the surrounding prism is usually the *axis-aligned bounding box* of the object. All the operators common to two-dimensional convolutional networks are extended in the obvious way.

A glaring problem is that the space and computational requirements grow *cubically* instead of quadratically as in the case of images. For example: An image with a resolution of 256 by 256 pixels would need 65536 pixels, while a voxelised 3D model with a resolution of $256^3$ would have 16777216 voxels! If every voxel takes up one 32-bit machine word to store a feature, for example the normal at that point, then a single object would take around half a gigabyte of space. This is clearly impractical on modern computer architectures.

However, as shown on the seminal paper about **VoxNet**[@Maturana], even at low resolutions the network achieved good results, classifying with an accuracy of 92% on ModelNet10 and 83% on ModelNet40 at a resolution of $30^3$.

![In VoxNet[@Maturana2015], we process the point cloud of each object into its *occupancy grid*, where each voxel contains a positive value if it contains at least one point, at a given resolution. Zero otherwise. From then on, it is processed exactly analogous as a 2D convolutional network. The notation `Conv(32, 5, 2)` signifies a convolution kernel of size 5 and stride 2.](img/voxnet.png){ width=50% }

## First Attempt at Classification and Limitations
As we saw in the VoxNet[@Maturana2015] paper, the voxel-based methods seem the most general and accurate on the ModelNet datasets. We use a similar dense voxel network on the screws data set and get the following results:

![The pink dashed line is the training accuracy, the cyan line is the evaluation accuracy, of using a dense voxel-based network.](img/screws-acc.png){ width=70%}

We only ran the tests up to a resolution of $32^3$ as suggested by Maturana[@Maturana2015], since after that resolution, memory resources on the GPU become a real problem. As we can see in the image, the highest we could get as an evaluation accuracy of around $0.72$, which leaves a lot to be desired. To see why this may be the case, let's look at a voxelised screw:

![The same screw voxelised at different resolutions. Even at lower one, we can tell it is, in fact, a screw. However, it is not until more detailed resolutions where a human can distinguish whether it is a flat head or a round head screw.](img/screw-vox.png){ width=100%}


An important characteristic of screws is the *roundedness* of the head, and this only starts to show at higher resolutions. Since using a denser grid is problematic, we clearly need an alternative. This leads us to octree-based networks, which we will cover in the next chapter.
