# TensorFlow-Based Octree-CNN

In this chapter we will describe some implementation details of the TensorFlow port. We chose to do this in TensorFlow (TF) because of the high degree of extensibility it provides, its widespread adoption, its large ecosystem and supporting tools, as well as the possibility of integrating the package into the institute's own Machine Learning middleware tool CAEML.

## TensorFlow {#sec:TF}

*TensorFlow* is an open-source (Apache 2.0 license) symbolic math library used for machine learning applications, based on the dataflow programming paradigm, released by Google in 2015.

The framework offers an *API* for many programming languages, including *Python*, *C*, *C++*, *Go*, *Java*, and without official support through third-party libraries: *C#*, *Haskell*, *Julia*, *R*, *Scala*, *Rust*, and *OCaml*. Officially, the main supported languages are *Python*, and *C/C++* for low-level extensions, which are the languages we used for this thesis.

As the name implies, the main conceptual object and datatype in a *TensorFlow* program is a **tensor**, i.e. a multidimensional array, where the **order** of the tensor is the dimensionality of the array needed to represent it, or equivalently, the number of indices required for each component. As we will see in the Section about our implementation, the restriction to this datatype brought some difficulties porting the *O-CNN* implementation[@Wang2017] from *Caffe*, but also an advantage handling batched learning, as that simply means another dimension for the array, and doesn't have to be handled explicitly as in the original implementation.

This choice of datatype also makes parallelisation easy, and the *Python* implementation directly supports execution of the computations on both CPUs and GPUs (and other custom processors). To take advantage of this, our implementation includes the CUDA code necessary to run on a GPU.

We decided on using this framework because it is the most wildly used in the machine learning community, it is extensible and configurable, is for the most part well documented, and it has the accompanying software *Tensorboard* which provides nice monitoring and visualisation of programmable attributes of the computation graph, such as intermediate values, training and testing error, loss function, inner neurons values, etc.

## Acquiring the Data
As we mentioned in the introduction @sec:Motivation, we need vast amounts of mechanical parts labeled and classified 3D models to train our deep learning networks. Unfortunately, there is no such standard data set. There are two main ways we can go about. First, we could generate the data ourselves. This was done by Wallrath[@Clemens] in his Master's thesis. There he needed rendered images from generated 3D models of mugs. To do so, he parametrised properties of the mugs, like handle shape, lower radius, curvature, etc. and generated the meshes according to some constraints.
While we could conceivably do the same for mechanical parts, we run into a problem for our specific use case. A weirdly shaped mug is still, arguably, a valid mug. This is not the case for machine parts, where each property is exactly measured, developed, and regulated, and changing them could render the model invalid, or at least not something you could realistically find.

This discards generating the models parametrically. It leaves finding pre-existing models somewhere and cleaning them up, sorting them, and pre-processing them. Feuillard[@Raul2018] decided on using the catalog of the important American supplier McMaster-Carr. They don't supply a database of their models, however, their website indexes all their parts and offers individual CAD models of each of them, which is exactly what we needed. We automated the acquisition process and wrote a web-scraper that goes through the website and downloads the models.
It should be noted that they offer more than half a million individual parts, some of which do not belong to any category apart from themselves. This is clearly unpractical, specially considering that downloading and organizing the files by hand takes time. We compromised on four major categories, where each would have at least 1000 different parts: screws, nuts, washers, and bolts.
Each of these major categories is then again divided into more specific categories. For example, `Flat Head Screws` is a type of `Screw`. These may be again subdivided into more specific types, and then once more differentiating measurement unit types and materials (Imperial or Metric units, military certifications, etc.). These last categorization order is not very important for voxel based classification.
These are all rather common and important mechanical parts, so we felt it was a good compromise.

TODO: Pictures of parts

Overall, our data set contains over 11000 distinct models, organized into 28 categories, some of which are also subdivided into 55 lower categories in total. Some categories like  certain screws contain multiple thousand items, while some washers only a couple hundred. This will allow us to investigate whether this affects the quality of classification, and the effects of data augmentation.


 | |
--- | --- | ---
| Screws  | Socket Head Screws          | Socket Head Screws                               |
|         |                             | Low-Profile Socket Head Screws                   |
|         |                             | Ultra Low-Profile Socket Head Screws             |
|         |                             | Vibration Resistant Socket Head Screws           |
|         |                             | Flanged Socket Head Screws                       |
|         |                             | Vented Socket Head Screws                        |
|         |                             | Torx Socket Head Screws                          |
|         |                             | Sealing Socket Head Screws                       |
|         | Flat Head Screws            | Hex Drive Flat Head Screws                       |
|         |                             | Phillips Flat Head Screws                        |
|         |                             | Slotted Flat Head Screws                         |
|         |                             | Torx Flat Head Screws                            |
|         |                             | Tamper Resistant Flat Head Screws                |
|         |                             | Vibration Resistant Flat Head Screws             |
|         |                             | Vented Flat Head Screws                          |
|         |                             | Sealing Flat Head Screws                         |
|         | Rounded Head Screws         | Hex Drive Rounded Head Screws                    |
|         |                             | Phillips Rounded Head Screws                     |
|         |                             | Slotted Rounded Head Screws                      |
|         |                             | Torx Rounded Head Screws                         |
|         |                             | Combination Phillips/Slotted Rounded Head Screws |
|         |                             | Flanged Rounded Head Screws                      |
|         |                             | Vibration Resistant Rounded Head Screws          |
|         |                             | Tamper Resistant Rounded Head Screws             |
|         | Hex Head Screws             | Hex Head Screws                                  |
|         |                             | Extra-Wide Hex Head Screws                       |
|         |                             | Flanged Hex Head Screws                          |
|         |                             | Hex Head Screws with Flat Washer                 |
|         |                             | Sealing Hex Head Screws                          |
|         | 12 Point Screws             | 12 Point Screws                                  |
|         | Set Screws                  | Cup Point Set Screws                             |
|         |                             | Non-marring Set Screws                           |
|         |                             | Extended Tip Set Screws                          |
|         | Thumb Screws                | Thumb Screws                                     |
|         |                             | Knobs                                            |
| Nuts    | Hex Nuts                    |                                                  |
|         | Locknuts                    |                                                  |
|         | Flange Nuts                 |                                                  |
|         | Thumb Nuts                  |                                                  |
|         | Coupling Nuts               |                                                  |
|         | Cap Nuts                    |                                                  |
|         | Square Nuts                 |                                                  |
|         | Press Fit Nuts              |                                                  |
| Washers | Flat Washers                |                                                  |
|         | Internal Tooth Lock Washers |                                                  |
|         | External Tooth Lock Washers |                                                  |
|         | Split lock Washers          |                                                  |
| Bolts   | Carriage and Plow Bolts     |                                                  |
|         | Elevator Bolts              |                                                  |
|         | L Hook Anchors              |                                                  |
|         | J Hook Anchors              |                                                  |
|         | T Slot Bolts                |                                                  |
|         | T Bolts                     |                                                  |

Table: Categories in the McMaster-Carr data set. TODO fix the layout

The files downloaded from the website come in the SolidWorks CAD file format (with extension `.SLDPRT`). This format contain a lot of information that will be useless for our purpose, such as material properties, so we convert those to the Open File Format (`.OFF`).
This format is a very simple representation of shapes made out of polygons. A valid file contains a header, a line with the number of vertices, faces and edges, followed by a three dimensional point per line given its 3D coordinates. The last segment is a face per line, described by the indices of its border vertices.
The popularity of the ModelNet dataset made this the *de facto* format in this area of research.

## Preprocessing the Data {#sec:Preprocessing}

Next, we need to actually voxelise the models. As Wang et al.[@Wang2017] showed, we get better results if instead of using only the cell occupancy as the feature vector, we use the normal of the contained face in the cell. To be sure we have the correct normal information, we convert the 3D model into a dense point cloud with oriented normals. To do this, Wang et al. use a ray shooting algorithm, where they place 14 virtual cameras on the truncated bounding cube of the objects (a cube, where the corners are truncated to fit the object) and shoot 16000 uniformly distributed rays from each camera. We can calculate the intersection point and normal and store these. We also ignore points that are inside the object, i.e. that are occluded by other points.

This process is very time consuming, but has the advantage that the voxelisation step is simpler and faster, which allows experimenting easier with different resolutions.
As we just mentioned, we then create the octree data structure from the point cloud by simply subdividing in octants and using the algorithm describing in Section @sec:Octree.
To augment the data and eliminate some bias, the objects are rotated into 12 different random orientations, and the octrees are based on the axis-aligned bounding box. This structure can be serialized into a binary format in a rather straight-forward manner:

> `total_nodes`    # Total amount of octants (int)
> `final_nodes`    # Amount of nodes in the finest layer (int)
> `max_depth`      # Height of the tree (int)
> `node_nums`      # Number of nodes per layer (int[depth + 1])
> `node_nums_acc`  # Prefix sum of `node_nums`, to make indexing easier later (int[depth + 2])
> `key_data`       # Shuffle keys corresponding to the nodes (int[total_nodes])
> `children_data`  # What we called the node labels (int[total_nodes])
> `data`           # Actual feature data, in our case, the normals (float[final_nodes*3])

Notice we renamed the *node labels* to `children_data` to avoid confusion with class labels.

Now that we have the voxelised data, we partition it following a 2:1 split into training and validation data. For easier handling, all the data samples are joined together into two `tfrecord` files, one for training and one for testing, which also contain label data and other meta-information. `tfrecord` is based on *Google's* *Protocol Buffer*, a binary serialization format. This plays very well with *TensorFlow*'s built-in facilities, and can be streamed into the computational graph on-demand, avoiding cluttering the IO pipe. Moreover, it contains operations for shuffling records on the fly and batching.

Of course, the actual content of each record, corresponding to one model, first has to be deserialised into the group of (*NumPy*) arrays from Section @sec:Octree, which completely describe the octree structure, and can be fed into our custom operators for training or evaluation. Each record can be then deserialized in a TensorFlow graph in the following manner:

```python
def parse_function(record):
  features = {
    'total_nodes': tf.FixedLenFeature((1), tf.int64),
    'final_nodes': tf.FixedLenFeature((1), tf.int64),
    'depth': tf.FixedLenFeature((1), tf.int64),
    'full_layer': tf.FixedLenFeature((1), tf.int64),
    'node_num_data': tf.VarLenFeature(tf.int64),
    'node_num_accu': tf.VarLenFeature(tf.int64),
    'key_data': tf.VarLenFeature(tf.int64),
    'children_data': tf.VarLenFeature(tf.int64),
    'data': tf.VarLenFeature(tf.float32),
    'label_one_hot': tf.FixedLenFeature((cats), tf.int64)
  }

  return pf = tf.parse_single_example(record, features)  # Parsed features
```

where `cats` is the number of categories (classes), `label_one_hot` is a *one-hot* vector, i.e. an array of length `cats` with `1` in the index of the correct class and `0` otherwise, and `pf` a dictionary with all the required tensors.

This can be used to construct a `Dataset` object in the following way:

```python
dataset = tf.data.TFRecordDataset([path_to_tfrecord_file])
dataset = dataset.map(parse_function)
```
This object provides convenient functions to shuffle, partition, and batch the data.


In summary, the whole processing pipeline looks like this:

![Processing Pipeline. From raw .OFF data to trained model.](img/pipeline.png){ width=70% }

where every rectangle represents a different script in the project, which feeds its result to the next stage, in such a way that we can reproduce the whole process with a single button press.

## Adding the New Operations

Now we have got the data and a nice set up pipeline, we can start the training stage. But to be able to do that, we need to add to TensorFlow the operations we described before @sec:Octree-Ops. Normally, TensorFlow suggests creating new operations out of pre-existing operations, and while the default `conv` operator is very generic, it won't fit our needs since we need to use our own indexing system into the data structure. Therefore, we will need to write our own operations from scratch in `C/C++`. The required operations will be `octree_convolution`, `octree_pooling`, and `octree_to_full_layer`.

For illustration purposes, we will only describe the convolution operator.

To start writing our own operator, in a new `.cc` file, we first need to register it. TensorFlow offers a macro just for this purpose. This also serves to describe the inputs, outputs, their types, and additional attributes:

```c
REGISTER_OP("OctreeConv")
    .Input("input: T")
    .Input("kernel: T")
    .Input("final_nodes: int64")
    .Input("key_data: int64")
    .Input("children_data: int64")
    .Input("node_num_data: int64")
    .Input("current_depth: int64")
    .Input("strides: int64")
    .Output("output: T")
    .Attr("T: {float} = DT_FLOAT");
```

The type of the input features and the kernel are templated, so they can remain generic (for example, use `int` for occupancy, and `float` for normals), with `float` as the default.

TensorFlow is rather strict with its naming conventions. The name of the operator must be in capital camel-case, which will we transpiled into snake-case (therefore `octree-conv`). We can associate the operator name with the implementation class as follows:

```c
REGISTER_KERNEL_BUILDER(
    Name("OctreeConv")
    .Device(DEVICE_CPU)
    .TypeConstraint<float>("T"),
    OctreeConvOp<float>);
```

assuming that `OctreeConvOp` is the desired class.

Now we can start writing the actual class, which will have to inherit from `OpKernel`. The important method we have to implement is `void Compute(OpKernelContext* context)`. With the `OpKernelContext` parameter object we can access environment parameters, operation attributes, as well as the inputs. To get an input, we simply do:

```c
const Tensor& input_tensor = context->input(0);
auto input = input_tensor.flat<T>();
```

The inputs are indexed in the same order as they are declared in the registration step. The `Tensor` returned we can use to query things like its shape and size, while its `flat` version is a rather opaque array-like object which we can more easily manipulate.
Note that the `Compute` function returns `void`, so how do we return the output tensor to the calling program? The same `context` we used to get the input tensors also allows us to allocate memory for the output tensors, like so:

```c
Tensor* output_tensor = NULL;
OP_REQUIRES_OK(context, context->allocate_output(0, output_shape,
                                                   &output_tensor));
auto output = output_tensor->flat<T>();
```

The `0` in the allocation function is the index of the output, in case that we have more than one. The call is surrounded by the `OP_REQUIRES_OK` macro, which checks if the allocation was successful, and if not, print a nice error message instead of dumping the core in a `SEGFAULT`.

One last interesting step is how to actually perform the convolution. Since actually moving a virtual kernel window is rather slow and doesn't take advantage of memory coalescence, there is a faster method by reducing the convolution into a simple matrix multiplication. This multiplication can be done very fast using highly optimised functions like `GEMM` from the *Eigen* software library[@Dauphin2015a]. The first step to do this, is to "unfold" the octree structure into a matrix, by making each "patch" of would-be kernel into a row.

![Unfolding a single 2D image into a matrix for GEMM. Each patch is the height and width of the kernel, and the depth is the number of input feature maps. Note that the new representation is indeed redundant if the stride is less than the kernel size, nevertheless, this still brings a speedup.](img/im2col.png){ width=75% }

This process is often called `im2col` (image to column) stemming from the original *MatLab* implementation[@LeCun1998]. The figure depicts the function for a 2D image. For our octree, the implementation is in principle the same, but we have to take care of the indexing.

```c
for(int c = 0; c < in_depth; ++c) { // Number of input feature maps
	for(int k = 0; k < kernel_size; ++k) {
		for(int h = 0; h < out_size; ++h) { // Size of the output vector (the same as the input if stride == 1)

			const int index = stride == 2 ? (h << 6) + ni3[k] :
				(h >> 3 << 6) + ni3[(h % 8) * kernel_size + k];

			const int p = neigh[index]; //neigh indexes the neighbouring voxels

			data_col[(c*kernel_size + k)*out_size + h] = p == -1 ?
				0 : input(c*octree_h + p);
		}
	}
}
```

Here `neigh` indexes the neighbouring voxels, and `ni3` indexes the neighbours according to the kernel window (in this case of size `3`). For more details on how to calculate these index arrays, see the Appendix.

Now that we have a flat representation of the octree, we can treat it as a matrix and multiply it with the kernel:

```c
auto data_col_eigen = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>::Map(data_col.data(), out_size, in_depth * kernel_size);

auto kernel_flat = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>::Map(kernel_tensor.flat<float>().data(), in_depth * kernel_size, out_depth);

auto out_eigen = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>::Map(output.data(), out_size, out_depth);

out_eigen.noalias() = data_col_eigen * kernel_flat; // FINAL RESULT
```

Note the use of `noalias` to avoid the creation of intermediate structures. We can easily check that the dimensions match. Also note that the output vector still follows the structure of the octree and can be indexed accordingly without further processing.

![Final matrix multiplication equivalent to a convolution.](img/gemm.png){ width=75% }
