(TeX-add-style-hook
 "thesisclass"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrbook" "a4paper" "11pt" "titlepage")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("graphicx" "pdftex") ("textpos" "absolute" "overlay") ("babel" "english" "ngerman") ("algorithm2e" "boxruled" "german" "linesnumbered") ("hyperref" "raiselinks=true" "						bookmarks=true" "						bookmarksopenlevel=1" "						bookmarksopen=true" "						bookmarksnumbered=true" "						hyperindex=true" "						plainpages=false" "						pdfpagelabels=true" "						pdfborder={0 0 0.5}" "						colorlinks=false" "						linkbordercolor={0 0.61 0.50}" "						citebordercolor={0 0.61 0.50}")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "scrbook"
    "scrbook11"
    "fontenc"
    "inputenc"
    "ae"
    "graphicx"
    "vmargin"
    "fancyhdr"
    "url"
    "textpos"
    "tikz"
    "babel"
    "algorithm2e"
    "hyperref"
    "biblatex")
   (TeX-add-symbols
    '("changefont" 3)
    "chapterheadfont"
    "blankpage"
    "oldtableofcontents")
   (LaTeX-add-lengths
    "chapnolen"
    "chapparlen")
   (LaTeX-add-saveboxes
    "chapno"))
 :latex)

