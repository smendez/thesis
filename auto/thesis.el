(TeX-add-style-hook
 "thesis"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "definition"
    "titlepage"
    "chapter/motivation"
    "chapter/problem"
    "chapter/octree-based"
    "chapter/tensorflow-ocnn"
    "chapter/experiments"
    "thesisclass"
    "thesisclass10"
    "booktabs"
    "longtable"
    "float"
    "listings"
    "framed"
    "graphicx"
    "xcolor"
    "subfig")
   (TeX-add-symbols
    '("passthrough" 1)
    "tm"
    "reg"
    "tabularnewline"
    "myname"
    "mytitle"
    "myinstitute"
    "reviewerone"
    "reviewertwo"
    "advisor"
    "timestart"
    "timeend"
    "submissiontime")
   (LaTeX-add-environments
    "origfigure")
   (LaTeX-add-bibliographies
    "citations/MA.bib"))
 :latex)

