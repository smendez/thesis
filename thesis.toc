\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Machine Learning in CAE}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Outline}{2}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Classifying 3D Mechanical Parts}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Screws as an example}{3}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Convolutional Neural Networks}{3}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Convolutional Methods for 3D Shapes}{5}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Multi-View Approaches}{6}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Surface Retopologisation and Reparametrisation}{7}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Voxel-Based Methods}{9}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}A First Unsuccessful Attempt}{10}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Octree-Based Convolutional Neural Networks}{13}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Octree}{14}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Building the Octree}{14}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Operations on the Octree}{16}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}TensorFlow-Based Octree-CNN}{19}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}TensorFlow}{19}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Acquiring the Data}{20}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Preprocessing the Data}{22}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Adding the New Operations}{24}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Evaluating the Network}{29}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}ModelNet Datasets}{31}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{35}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Future Work}{36}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Figures}{38}{chapter*.24}
